#include <iostream>
#include <vector>
#include <algorithm>

namespace geometry {
    struct point {
        point() : x(0), y(0) {}

        point(int64_t x, int64_t y) : x(x), y(y) {}

        point(point const &rhs) = default;

        ~point() = default;

        bool operator<(point const &rhs) const {
            return (x < rhs.x) || (x == rhs.x && y < rhs.y);
        }

        friend std::istream &operator>>(std::istream &in, point &rhs) {
            in >> rhs.x >> rhs.y;
            return in;
        }

        [[nodiscard]] int64_t get_x() const {
            return x;
        }

        [[nodiscard]] int64_t get_y() const {
            return y;
        }

     private:
        int64_t x, y;
    };

    int64_t sqr(int64_t a) {
        return a * a;
    }

    uint64_t get_dist(point const &p1, point const &p2) {
        return sqr(p1.get_x() - p2.get_x()) + sqr(p1.get_y() - p2.get_y());
    }

    uint64_t __get_minimal_dist(std::vector<point> &points, size_t l, size_t r) {
        if (r - l <= 3) {
            std::sort(points.begin() + l, points.begin() + r,
                      [](auto &p1, auto &p2) { return p1.get_y() < p2.get_y(); });
            uint64_t ans = INT64_MAX;
            for (size_t i = l; i < r; i++) {
                for (size_t j = i + 1; j < r; j++) {
                    ans = std::min(ans, get_dist(points[i], points[j]));
                }
            }
            return ans;
        } else {
            size_t mid = (l + r) / 2;
            point mid_point = points[mid];
            uint64_t ans = std::min(__get_minimal_dist(points, l, mid), __get_minimal_dist(points, mid, r));
            std::inplace_merge(points.begin() + l, points.begin() + mid, points.begin() + r, [](auto &p1, auto &p2) {
                return p1.get_y() < p2.get_y();
            });
            for (size_t i = l; i < r; i++) {
                if (static_cast<uint64_t>(std::abs(points[i].get_x() - mid_point.get_x())) < ans) {
                    for (ptrdiff_t j = i - 1; j >= 0 && j >= static_cast<ptrdiff_t>(l) &&
                                              static_cast<uint64_t>(points[i].get_y() - points[j].get_y()) < ans; j--) {
                        ans = std::min(ans, get_dist(points[i], points[j]));
                    }
                }
            }
            return ans;
        }
    }

    uint64_t get_minimal_dist(std::vector<point> &points) {
        std::sort(points.begin(), points.end());
        return __get_minimal_dist(points, 0, points.size());
    }
}

int main() {
    std::ios_base::sync_with_stdio(false);
    size_t n;
    std::cin >> n;
    std::vector<geometry::point> points(n);
    for (size_t i = 0; i < n; i++) {
        std::cin >> points[i];
    }
    std::cout << geometry::get_minimal_dist(points) << std::endl;
    return 0;
}
